import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    Text
  } from 'react-native';

const MusicItemsScreen = ( {navigation, route} ) => {

    const [items, setItems] = useState ([]);

    const getSongs = async () => {
        // call api
        const response = await fetch("http://localhost:3000/songs");
        const songs = await response.json();
        setItems(songs);
    };

    useEffect ( () => {getSongs()}, []);

    const MucicItem = ({song}) => {
        return (
            <View style={{ 
                margin: 5,
                backgroundColor: "#FFDFFD",
                }}>
                <Text> Title: {song.title} </Text>
                <Text> Artist: {song.artist} </Text>
                <Text> Link: {song.link} </Text>
            </View>
        );
    };

    return (
    <SafeAreaView> 
        {
            items.map((value, index) => {
                    return <MucicItem key={value.id} song={value}/>
                }
            )
        }
        
    </SafeAreaView>
    );
}

export default MusicItemsScreen;