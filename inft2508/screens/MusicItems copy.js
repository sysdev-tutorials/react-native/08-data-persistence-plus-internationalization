import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    SectionList,
    Text,
  } from 'react-native';

const MusicItemsScreen = ( {navigation, route} ) => {

    // const {category} = route.params;
    const [items, setItems] = useState ([]);

    const getSongs = async () => {
        if (category == "Music") {
            // call api
            const response = await fetch("http://localhost:3000/songs");
            const songs = await response.json();
            setItems(songs);
        }
    };

    useEffect ( () => {getSongs()}, []);

    return (
    <SafeAreaView> 
        <View style={{ 
            paddingTop: 20
            }}>
            <Text> Hello Music </Text>
        </View>
    </SafeAreaView>
    );
}

export default MusicItemsScreen;