import React, { useState } from 'react';
import ColorPicker from 'react-native-wheel-color-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'; 

const SettingsScreen = ( {navigation, route} ) => {

    // set default color for the color picker
    const [cardbgcolor, setCardbgcolor] = useState ('#416fff');
    
    // on color change, persist it
    const onColorChangeComplete = async (color) =>{
        try {
            await AsyncStorage.setItem('cardbgcolor', color)
        } catch (e) {
            // todo handle error
        }
    }

    return (
        <ColorPicker
            color={cardbgcolor}
            onColorChangeComplete={onColorChangeComplete}
            style= {{margin: 20}}
        />
    );

}

export default SettingsScreen;