import React, {useState, useEffect} from 'react';
import { 
    View, 
    SafeAreaView,
    Text,
    Image,
    Pressable
  } from 'react-native';

const MusicItemsScreen = ( {navigation, route} ) => {

    const [items, setItems] = useState ([]);
    const [refresh, setRefresh] = useState(false);

    const getSongs = async () => {
        // call api
        const response = await fetch("http://localhost:3000/songs");
        const songs = await response.json();
        setItems(songs);
    };

    const addSong = async () => {

        // random number between 0 and 1000
        const randomNumber = Math.floor(Math.random() * 100000);

        // call api
        const response = await fetch("http://localhost:3000/songs",
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify ({
                    id: randomNumber,
                    title: 'song ' + randomNumber + ' title',
                    artist: 'artist ' + randomNumber,
                    link: 'youtube.com/' + randomNumber
                })
            });
        const songAdded = await response.json();
        setRefresh(true);
    };

    useEffect ( () => {
        getSongs();
        return function cleanup(){
            setRefresh(false);
        }
    }, [refresh]);

    const MucicItem = ({song}) => {
        return (
            <View style={{ 
                margin: 5,
                backgroundColor: "#FFDFFD",
                }}>
                <Text> Title: {song.title} </Text>
                <Text> Artist: {song.artist} </Text>
                <Text> Link: {song.link} </Text>
            </View>
        );
    };
    const MusicHeader = () => {
        return (
            <View style={{ 
                padding: 5,
                alignItems: "center",
                justifyContent: "center",
                }}>
                <Pressable onPress ={addSong}>
                    <Image 
                        style= {{width: 40, height: 40}} 
                        source={require('../images/plus-sign.png')}
                    />
                </Pressable>
            </View>
        );
    };

    return (
    <SafeAreaView> 
        <MusicHeader/>
        {
            items.map((value, index) => {
                    return <MucicItem key={value.id} song={value}/>
                }
            )
        }
        
    </SafeAreaView>
    );
}

export default MusicItemsScreen;