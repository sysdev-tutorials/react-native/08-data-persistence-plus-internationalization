import React, {useState, useEffect} from 'react';
import { 
  Text, 
  View, 
  Image,
  Pressable,
 } from 'react-native';

 import AsyncStorage from '@react-native-async-storage/async-storage'; 


const Card = (props) => {

  // initialize
  const [preferredCardColor, setPreferredCardColor] = useState('#9FE8FF');

  // update
  useEffect( () => {
    updateCardColor();
  }, []);

  const updateCardColor = async() => {
    try {
      const value = await AsyncStorage.getItem('cardbgcolor')
      if(value !== null) {
        // value previously stored
        setPreferredCardColor(value);
      }
    } catch(e) {
      // todo handle error
      return null;
    }
  }

  const onCardPress = () => {
   props.showItems(props.displayText)
  }
  
  return (
    <Pressable onPress ={onCardPress}>
      <View style={{ 
            backgroundColor: preferredCardColor,
            height: 100,
            width: 150,
            borderRadius: 10,
            justifyContent: "center", 
            alignItems: "center",
            margin: 10,
          }}>
          <View style= {{flex: 1, justifyContent: "flex-end"}}>
            <Image
              style= {{width: 40, height: 40}}
              source={props.logo}
            />
          </View>
          <View style= {{flex: 1, justifyContent: "flex-start"}}>
            <Text>{props.displayText}</Text>
          </View>
      </View>
    </Pressable>
  );
}

export default Card;