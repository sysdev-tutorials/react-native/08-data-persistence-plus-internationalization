import React from 'react';
import {
  View, 
  Pressable,
  Image
 } from 'react-native';

 const SettingsIcon = (props) => {

    const onSettingsIconPress = () => {
        console.log(props);
       }
       
       return (
        <Pressable onPress={onSettingsIconPress}>
            <View>
                <Image
                style= {{width: 20, height: 20, marginRight: 20}}
                source={require('../images/logo-settings.png')}
                />
            </View>
      </Pressable>
       );
 }

 export default SettingsIcon;