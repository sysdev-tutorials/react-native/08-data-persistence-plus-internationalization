import React, {useState} from 'react';

import { 
  View,
  Pressable,
  Image
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/Home';
import FoodItemsScreen from './screens/FoodItems';
import MusicItemsScreen from './screens/MusicItems';

const Stack = createNativeStackNavigator();

const Inft2508App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options= {{
            headerRight: () => (
              <Pressable>
                <View>
                  <Image
                  style= {{width: 20, height: 20, marginRight: 20}}
                  source={require('./images/logo-settings.png')}
                  />
                </View>
              </Pressable>
            )
          }}
          />
        <Stack.Screen
          name="FoodItems"
          component={FoodItemsScreen}
        />
        <Stack.Screen
          name="MusicItems"
          component={MusicItemsScreen}
        />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Inft2508App;