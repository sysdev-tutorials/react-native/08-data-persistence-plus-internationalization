import React, {useState} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/Home';
import FoodItemsScreen from './screens/FoodItems';
import MusicItemsScreen from './screens/MusicItems';
import SettingsIcon from './components/SettingsIcon';

const Stack = createNativeStackNavigator();

const Inft2508App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options= {{
            headerRight: () => (
              <SettingsIcon/>
            )
          }}
          />
        <Stack.Screen
          name="FoodItems"
          component={FoodItemsScreen}
        />
        <Stack.Screen
          name="MusicItems"
          component={MusicItemsScreen}
        />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Inft2508App;