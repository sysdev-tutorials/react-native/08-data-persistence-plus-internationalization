# Data Persistence Plus Internationalization

In this lesson we will look into two main topics - data persistence and app localization.

- Data persistence: allows app to save or store application related data locally into the device, not into application´s memory. That means if user restarted the app, the persisted data can be retrieved and used in the app. Common usecase for this is to allow users set preferences or settings in the app.
- Localization: allows app to use texts/string in the application´s user-interface  based on the language settings in the device. For example if language setting in the device is 'norwegian' then application will display its texts in norwegain language and so on.

As in the previous lessons, we will learn these concepts by improving our app that we have been working in previous lessons.

# Preparations

If you do a fresh start of a repo, copy inft2508 folder from previous lesson (https://gitlab.com/sysdev-tutorials/react-native/07-communication-with-outside-world/-/tree/main/inft2508) to your repo!

Recap: Af this stage, if you run the app together with json-server as API server, including other features are multiple pages.

# Application data persistence
In order to understand the concept of application data persistence, we need a scenario to work with. For the demo purposes we are going to implement the following relatively simple scenario.

>Scenario: Currently the 'Card' component of our application has fixed background color. What if we allow user to change and use their preferred backgdound color! This can for example be done by adding a setting menu in the top navigation bar of the home screen, that will nagivate to a new settings screen where user can change the background color for the card component. When the preferred color is changed it is persisted. When application restarts, it first tries to read the preferred color from the persitent storage and if use that as background color for the card component. If color is not found in the persistent storage, default 'hardcoded' color will be used!

## Solution for data persistence in react-native
There are multiple ways for data persistence in react-native apps. The recommended solution is to use a commnity developed package called 'react-native-async-storage'. Check out the official like for the package here, https://github.com/react-native-async-storage/async-storage

### Installation of react-native-async-storage
You are encouraged to visit the official link above to see detailed installation instructions. In summary, you need to run the following commands from the project directory (inft2508).

```
npm install @react-native-async-storage/async-storage
cd ios && npx pod-install
```

### Usage of react-native-async-storage
Usage is quite simple. See official documentation here https://react-native-async-storage.github.io/async-storage/docs/usage.

First, as usual, 'AsyncStorage' component need to be imported from the package. And then depending on the use case, one can use ```setItem(key, value)``` method to store date to the storage, or ```getItem(key)``` method to retrieve the stored data from the storage.

Note however that both  ```setItem``` and ```getItem``` methods return promise. That means they are asynchronous operations and need to be used either using ```chain``` pattern or using ```async/await``` pattern.

Lets see them in action!

For persisting data,

```
const storeData = async (value) => {
  try {
    await AsyncStorage.setItem('key', value)
  } catch (e) {
    // handle error
  }
}
```

For reading presisted data
```
const getData = async () => {
  try {
    const value = await AsyncStorage.getItem('key')
    if(value !== null) {
      // value previously stored
    }
  } catch(e) {
    // handle error
  }
}
```

### Usage of react-native-async-storage in our app

We will use the scenario described above to demonstrate the use of  react-native-async-storage package in our app. So, first add a 'Settings' like icon at the top right of the 'Home' screen navigation header! Pressing on that will lead to a new 'Settings' screen (to be implemented yet).

### Creating SettingIcon component
Lets create a reusable component 'SettingsIcon' under 'components/SettingsIcon.js' with the following contents. Note that current implementation of 'onSettingsIconPress' event handler does basically nothing, just prints out the properties passed to the component.

<img src="topRightSettingsIcon.png" align="right" width=250>

```
// contents of components/SettingsIcon.js file

import React from 'react';
import {
  View, 
  Pressable,
  Image
 } from 'react-native';

 const SettingsIcon = (props) => {

    const onSettingsIconPress = () => {
        console.log(props);
       }
       
       return (
        <Pressable onPress={onSettingsIconPress}>
            <View>
                <Image
                style= {{width: 20, height: 20, marginRight: 20}}
                source={require('../images/logo-settings.png')}
                />
            </View>
      </Pressable>
       );
 }

 export default SettingsIcon;
```

<br clear="both">


### Using SettingIcon component as navigator bar icon
Next, we need to use this 'SettingsIcon' component in our app - for example as right header icon for the 'Home' screen. With react-nagivation package, we can do this using 'options' property of the 'Stack.Screen' component in 'App.js' file, like shown below.


```
// contents of App.js file

import React, {useState} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/Home';
import FoodItemsScreen from './screens/FoodItems';
import MusicItemsScreen from './screens/MusicItems';
import SettingsIcon from './components/SettingsIcon';

const Stack = createNativeStackNavigator();

const Inft2508App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options= {{
            headerRight: () => (
              <SettingsIcon/>
            )
          }}
          />
        <Stack.Screen
          name="FoodItems"
          component={FoodItemsScreen}
        />
        <Stack.Screen
          name="MusicItems"
          component={MusicItemsScreen}
        />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Inft2508App;
```

Now, if you run and inspect the app, you will notice that the settings icon is displayed as right header for the Home screen navigation bar. However when you press it, there is no changes in the UI happens in the app. Note also that so far we are not passing any properties to the 'SettingsIcon' component from App.js file!

So, next thing that we would like to do is navigate to the new screen when user presses on the 'SettingsIcon'. For that, first we need to implement a new 'SettingsScreen' and register it to the 'Stack.Navigator' component of react-nagivation.

### Creating and registering a SettingsScreen 
Lets create our Settings screen (screens/SettingsScreen.js) and register it. 

Screenshot for the SettingsScreen is shown below. For the demonstration purpose, our settings screen will be like a simple color picker! Whenever user selected a new color, it will be saved or persisted.

We will will a thirdparty library called 'react-native-wheel-color-picker'​. More information about this library can be found in here https://www.npmjs.com/package/react-native-wheel-color-picker.

Install 'react-native-wheel-color-picker' by running these commands from the project folder 

```
npm i react-native-wheel-color-picker
cd ios && npx pod-install
```

Then, import 'ColorPicker' in SettingsScreen.js and use it inside return block​, as shown in the code snippet for SettingsScreen is below.


<img src="settingsscreenColorpicker.png" align="right" width=200>

```
// contents for SettingsScreen.js

import React, { useState } from 'react';
import ColorPicker from 'react-native-wheel-color-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'; 

const SettingsScreen = ( {navigation, route} ) => {

    // set default color
    const [cardbgcolor, setCardbgcolor] = useState ('#416fff');
    
    // on color change, persist it
    const onColorChangeComplete = async (color) =>{
        try {
            await AsyncStorage.setItem('cardbgcolor', color)
        } catch (e) {
            // todo handle error
        }
    }

    return (
        <ColorPicker
            color={cardbgcolor}
            onColorChangeComplete={onColorChangeComplete}
            style= {{margin: 20}}
        />
    );

}

export default SettingsScreen;
```

<br clear="both">

Now we have 'SettingsScreen', and lets register it to 'Stack.Navigator'. The code snippet for that is below i.e. the contents of 'App.js' file.


```javascript showLineNumbers
// contents of App.js file
import React, {useState} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/Home';
import FoodItemsScreen from './screens/FoodItems';
import MusicItemsScreen from './screens/MusicItems';
import SettingsIcon from './components/SettingsIcon';
import SettingsScreen from './screens/SettingsScreen';

const Stack = createNativeStackNavigator();

const Inft2508App = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options= { ({navigation}) => ({
            headerRight: () => (<SettingsIcon navigation={navigation}/>)
          })}
          />
        <Stack.Screen
          name="FoodItems"
          component={FoodItemsScreen}
        />
        <Stack.Screen
          name="MusicItems"
          component={MusicItemsScreen}
        />
        <Stack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
        />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Inft2508App;

```

All right, we have not created and registered our SettingsScreen. If you run and inspect your app, you still not be able to navigate from 'Home' screen header to the new 'SettingsScreen'. Lets fix that in next section!

### Navigating to 'SettingsScreen'
Previously in 'SettinsIcon' component we have a event handler function ```onSettingsIconPress``` but there was no logic to navigate to 'SettingsScreen'. So, lets update that and have correct nagivation logic. Update code snippet for ```SettingsIcon.js``` is below.

```javascript showLineNumbers
// Updated contents for SettinsIcon.js file
import React from 'react';
import {
  View, 
  Pressable,
  Image
 } from 'react-native';

 const SettingsIcon = (props) => {

    const onSettingsIconPress = () => {
        props.navigation.navigate('SettingsScreen');
       }
       
       return (
        <Pressable onPress={onSettingsIconPress}>
            <View>
                <Image
                style= {{width: 20, height: 20, marginRight: 20}}
                source={require('../images/logo-settings.png')}
                />
            </View>
      </Pressable>
       );
 }

 export default SettingsIcon;

```

As you have noticed in the code snippet, we have used a ```navigation``` property´s ```nagivation``` function to navigate to 'SettingsScren'.

Note also also in 'App.js' file we have passed ```navigation``` as property to 'SettingsIcon' component.

If you run the app and inspect, you should be also be navigate settings screen from home screen nagivator! We have also implemented that whenerver color is changed in settings screen, it is persisted. However we have not yet retrieved the persisted value and use it in the Card component (or in the Home screen). Lets fix this next!

### Reading and using persisted data 
Lets update our 'Cards.js' component. 

First import 'AsyncStorage' component from 'async-storage' package and then read the persisted data when the 'Card' component is mounted for the first time! This can be achieved, as before, by using 'useEffect' hook. Note that 'getItem' method of AsyncStorage component is an async operation therefore we need to use 'useEffect' hook provided by react-native.

The code snippet for updated 'Card.js' file is given below. Specifically, note how 'updateCardColor' method and 'useEffect' hook are used. Note also that top level 'View' component uses variable as background color! They are newly added or updated.

```javascript showLineNumbers
// Contents of Card.js file

import React, {useState, useEffect} from 'react';
import { 
  Text, 
  View, 
  Image,
  Pressable,
 } from 'react-native';

 import AsyncStorage from '@react-native-async-storage/async-storage'; 


const Card = (props) => {

  // initialize
  const [preferredCardColor, setPreferredCardColor] = useState('#9FE8FF');

  // update
  useEffect( () => {
    updateCardColor();
  }, []);

  const updateCardColor = async() => {
    try {
      const value = await AsyncStorage.getItem('cardbgcolor')
      if(value !== null) {
        // value previously stored
        setPreferredCardColor(value);
      }
    } catch(e) {
      // todo handle error
      return null;
    }
  }

  const onCardPress = () => {
   props.showItems(props.displayText)
  }
  
  return (
    <Pressable onPress ={onCardPress}>
      <View style={{ 
            backgroundColor: preferredCardColor,
            height: 100,
            width: 150,
            borderRadius: 10,
            justifyContent: "center", 
            alignItems: "center",
            margin: 10,
          }}>
          <View style= {{flex: 1, justifyContent: "flex-end"}}>
            <Image
              style= {{width: 40, height: 40}}
              source={props.logo}
            />
          </View>
          <View style= {{flex: 1, justifyContent: "flex-start"}}>
            <Text>{props.displayText}</Text>
          </View>
      </View>
    </Pressable>
  );
}

export default Card;

```

Now,you can run and inspect the app by performing the following steps.

- Note that 'Cards' in the Home screen will have a default color (for the first time when the app starts)
- You click on the 'Settings' icon located at top-right of Home screen´s navigataion bar. That will take you to the 'Seetings' screen
- Pick any colour you like in Color picker available in the settings screen. 
- Thats it, exit the app
- Restart the app and now you will see the background color of the 'Card' components in the Home screen is not the default color but the color you have picked in the settings screen!


That´s it. Congratulations, you have successfully persisted data from the app and used it!

In the next section we will look into app localization!


# App localization

Mobile app localization refers to the process of adopting your app to different languages, cultures and markets. In this lesson we will focus on adapting your mobile app to different languages for example based on language setting on the device!

>Scenario: We will translate texts displayed in the 'Home' screen cards in to three (or more) different languages. The texts from correct languages will be used in the app based on the language settings in the device. For example if language setting is 'norwegian', the norwegian language translated texts will be used, if anguage setting is 'spanish', the spanish language translated texts will be used and so on. Note the we will also use the concept of fallback language. That means if texts are not found in the preferred language, texts from fallback language will be used.

Lets get into action!

## Packages used for app localization
The state-of-art recommended packages (or libraries) for react-native app location are react-native-localize (https://github.com/zoontek/react-native-localize​) and i18n-js (https://www.npmjs.com/package/i18n-js). In this lesson, we will use both of these packages together!

### Installations
Readers are encouraged to visit official websites for then installation of react-native-localize and i18n-js packages. Installation summary is given below.

```
npm install react-native-localize i18n-js​
cd ios && npx pod-install​
```

In order to localize the react-native app, we need to do the following steps

- Translate texts in to different target languages i.e preferably one translated texts file for each language or locale
- Then, import components from ​'react-native-localize' and 'i18n-js' packages, and configure them
- Display translated texts in the app

We will show the details of these steps in the following. For the sake of demo purpose, we will translate only the texts displated in 'Home' screen 'Card' components! We will also translate texts only in three languages - english (also as fallback language), norwegian and spanish. Note that the translation is done using google translate, so the translation texts may not be accurate!

So, lets create translated texts under 'localization/translations.json' file. Contents of this file are given below. Note that some texts are intentionally kept untranslated​ and we will come back to that later in the lesson!

Contents of 'localization/translations.json' file
```
{
    "en" : {
        "Food": "Food",
        "Music": "Music",
        "Vechiles": "Vechiles",
        "Real State": "Real State",
        "Dating": "Dating",
        "Mobile": "Mobiles"
    },
    "nb": {
        "Food": "Mat og drikke",
        "Music": "Musikk",
        "Vechiles": "Kjøretøy",
        "Dating": "Dating",
        "Mobile": "Mobil"
    },
    "es": {
        "Food": "Alimento",
        "Music": "Música",
        "Vechiles": "Vechiles",
        "Real State": "Bienes raíces",
        "Dating": "Tener una cita",
        "Mobile": "Móvil"
    }
}
```

Next, lets create a utity file to instantiate and configure 'I18n' component from 'i18n-js' package. Create a file 'localization/I18n.js' (note that name can be anything) with the following contents.

As you can notice, we importe 'I18n' from "i18n-js" package and configure it with translated texts, locale, default language and so on. Specifically note that it is configured only with translated texts for the detected locale!

```
// contents of I18n.js
import {I18n} from "i18n-js";
import translations from "./translations.json";

import * as RNLocalize from 'react-native-localize';
const locales = RNLocalize.getLocales();

// configure i18n
const i18n = new I18n({...translations});
i18n.locale = locales[0].languageCode;    
export default i18n;
```

Now that we have configured 'I18n'. Lets use the texts in the UI.

Go to the 'Card.js' component. Import our 'I18n' from '../localization/I18n' and then use the translated texts like ```I18n.t(key)```. The updated code snippet for 'Card.js' is given below. 


```
import React, {useState, useEffect} from 'react';
import { Text, View, Image, Pressable, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage'; 

import i18n from '../localization/I18n';

const Card = (props) => {

    // initialize
    const [preferredCardColor, setPreferredCardColor] = useState('#9FE8FF');

    // update
    useEffect(() => {
      updateCardColor();
    }, []);

    const myCardPressFunction = () => {
      props.showItems(props.displayText)
    }

    const updateCardColor = async() => {
      try {
        const value = await AsyncStorage.getItem('cardbgcolor')
        if(value !== null) {
          // value previously stored
          setPreferredCardColor(value);
        }
      } catch(e) {
        // todo handle error
        return null;
      }
    }
  
    return (
      <Pressable onPress ={myCardPressFunction}>
        <View style={{ 
              backgroundColor: preferredCardColor,
              height: 100,
              width: 150,
              borderRadius: 10,
              justifyContent: "center", 
              alignItems: "center",
              margin: 10,
            }}>
            <View style= {{flex: 1, justifyContent: "flex-end"}}>
              <Image
                style= {{width: 20, height: 20}}
                source={props.logo}
              />
            </View>
            <View style= {{flex: 1, justifyContent: "flex-start"}}>
              <Text>{i18n.t(props.displayText)}</Text>
            </View>
        </View>
      </Pressable>
      );
    
}

export default Card;
```

<img src="missing_tx.png" align="right" width=250>

If you run the app. Change the device language settings to for example 'Norwegian', and inspect the app (restart might be needed). You will see that 'Card' components in the home screen will be displayed in 'Norwegian'!

As you can see in the app screenshot, most of the texts are properly displayed in norwegian language but there are places where we see messages like  ```missing bla bla bla```! The reason for this is the our 'localization/translations.json' file does not contain all the translated texts or that ```I18n.t(key)``` function could find translated text for the given key. And also that we have not configured I18n about what to do in such circumstances!

One solution to get rid of such error is to configure a default language when translations for the given/preferred language does not exist. 
This can be done by adding these two line to the ```I18n``` configuration!
```
i18n.enableFallback = true;
i18n.defaultLocale = "en"; 
```

The updated ```I18n.js``` file contents is given below.

<br clear="both">

The updated code snipped for our 'I18n.js' file is below.

```
// contents of I18n.js
import {I18n} from "i18n-js";
import translations from "./translations.json";

import * as RNLocalize from 'react-native-localize';
const locales = RNLocalize.getLocales();

// configure i18n
const i18n = new I18n({...translations});
i18n.locale = locales[0].languageCode;
i18n.enableFallback = true;
i18n.defaultLocale = "en";
    
export default i18n;
```

<img src="use_fallback_tx.png" align="right" width=250>

If you run the app now and inspect the app with 'norwegian' as device languge settings, you will the app like below!

You can notice, there is no error message like messages like  ```missing bla bla bla``` for the missing translations. Instead texts from fallback language ( which is configured 'en' in this case) are displayed!

<br clear="both">

That´s it for translations! 

Congratulations, you have also now learned about app localization :)



# Exercise #7
This exercise will also be a follow-up exercise from previous exercises (upto #6). 

Specific tasks in this exercise are following:
- Persist application data into local device, for example via Settings screen
- Use persisted data when application starts (restarts)
- Translate all the strings/texts in your app into 2-3 differnent languages, and use those translated texts in the app according to the language settings in the device